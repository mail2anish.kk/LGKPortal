'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('AppCtrl', function ($scope, $routeParams, $compile, $http, $rootScope, $window, $location, $sce) {
    try {
    console.log("App ctrl called");
    console.log($routeParams.appid + " : " + $routeParams.pageid);
            $rootScope.appid = $routeParams.appid;
            $rootScope.pageid = $routeParams.pageid;
            var pageDefinition = function (appDefinition, pageId) {
                var pageDef;
                var i;
                for (i = 0; i < appDefinition.pages.length; i++) {
                    var tempPage = appDefinition.pages[i]
                    if (tempPage.pageid == pageId) {
                        //alert (tempApp.name);
                        pageDef = tempPage;
                        break;
                    }
                }
                return pageDef;
            };
            $scope.init = function(app, appid1, pageid1) {
                console.log("Init called : " + appid1 + " : " + pageid1);
                console.log(app.title);
                if(app.appdef) {
                    setTimeout(function () {

                        console.log(app.appdef.name);
                        var pagedef = app.appdef.pages[0];
                        if(appid1 == app.appdef.name) {
                            console.log("FOUND : " + appid1);
                            pagedef = pageDefinition(app.appdef, pageid1);
                        }
                        var appid = app.appdef.name;
                        $("#widget" + appid).html("");
                        console.log("page Def : " + pagedef);
                        var updatedprocessor = pagedef.pageprocessor.split("#appContent").join("#widget" + appid);
                        window.eval(updatedprocessor);
                        var pageProcessorName = 'pageprocessor' + pagedef.pageid;

                        if (window[pageProcessorName] != undefined) {
                            window[pageProcessorName](pagedef, $scope, $routeParams, $compile,
                                $http, $rootScope, $sce, $window, $location);
                            $scope.$apply();
                        }
                    },100);
                }
            }
          }catch(ex) {
            console.log(ex);
          }

  }).
  controller('HomeCtrl', function ($rootScope, $scope, $http, $location, $window, $route, $compile) {
    try {
          console.log("home called");
            $http.get("/api/metadata/sidemenu").success(function (data) {
                console.log(data);
                $rootScope.sidenav = data;
            }).error(function (data) {
                console.log("Error "  + data);
            });


            $rootScope.trustHtml = function(html) {
                return $sce.trustAsHtml(html);
            }
             $BODY = $('body'),
                $MENU_TOGGLE = $('#menu_toggle'),
                $SIDEBAR_MENU = $('#sidebar-menu'),
                $SIDEBAR_FOOTER = $('.sidebar-footer'),
                $LEFT_COL = $('.left_col'),
                $RIGHT_COL = $('.right_col'),
                $NAV_MENU = $('.nav_menu'),
                $FOOTER = $('footer');
            var setContentHeight = function () {
                // reset height
                $RIGHT_COL.css('min-height', $(window).height());

                var bodyHeight = $BODY.outerHeight(),
                    footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
                    leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
                    contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

                // normalize content
                contentHeight -= $NAV_MENU.height() + footerHeight;

                $RIGHT_COL.css('min-height', contentHeight);
            };

            $rootScope.sideClick = function($event) {
                console.log("clicked " + $event.target);
                var $li = $($event.target).parent();
                console.log($li);
                if ($li.is('.active')) {
                    $li.removeClass('active active-sm');
                    $('ul:first', $li).slideUp(function() {
                        setContentHeight();
                    });
                } else {
                    // prevent closing menu if we are on child menu
                    if (!$li.parent().is('.child_menu')) {
                        $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                        $SIDEBAR_MENU.find('li ul').slideUp();
                    }

                    $li.addClass('active');

                    $('ul:first', $li).slideDown(function() {
                        setContentHeight();
                    });
                }

            }
          }catch(ex) {
            console.log(ex);
          }

  }).
  controller('DashboardCtrl', function ($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
    // write Ctrl here
    console.log("DashboardCtrl called compile");
    var dbid= $routeParams.dbid;
    var dboard = null;
    console.log("dashboard");
    $http.get("/api/metadata/db").success(function (data) {
        console.log(data);
        $rootScope.dbdata = data;
    }).error(function (data) {
        console.log("Error "  + data);
    });
  }).
  controller('AdminCtrl', function ($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
    // write Ctrl here
    console.log("AdminCtrl called compile");
    $scope.selectApp = function() {
        alert ("called  " + $scope.seletedapp);
        $http.get("/api/kryptos/livedata/" + $scope.seletedapp).success(function (data) {
            console.log(data);
            $scope.selectedappdata = data;
        }).error(function (data) {
            console.log("Error "  + data);
        });

    }

    $http.get("/api/kryptos/listMyApps").success(function (data) {
        console.log(data);
        $scope.myapps = data;
    }).error(function (data) {
        console.log("Error "  + data);
    });
  });
