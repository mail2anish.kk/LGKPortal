
angular.module('portalApp.controllers', []).
    controller('HomeCtrl', ['$rootScope', '$scope', '$http', '$location', '$window', '$sce', '$route', '$compile',
        function ($rootScope, $scope, $http, $location, $window, $sce, $route, $compile) {
            console.log("home called");
            $http.get("/sidemenu.json").success(function (data) {
                console.log(data);
                $rootScope.sidenav = data;
            }).error(function (data) {
                console.log("Error "  + data);
            });


            $rootScope.trustHtml = function(html) {
                return $sce.trustAsHtml(html);
            }

            $BODY = $('body'),
                $MENU_TOGGLE = $('#menu_toggle'),
                $SIDEBAR_MENU = $('#sidebar-menu'),
                $SIDEBAR_FOOTER = $('.sidebar-footer'),
                $LEFT_COL = $('.left_col'),
                $RIGHT_COL = $('.right_col'),
                $NAV_MENU = $('.nav_menu'),
                $FOOTER = $('footer');
            var setContentHeight = function () {
                // reset height
                $RIGHT_COL.css('min-height', $(window).height());

                var bodyHeight = $BODY.outerHeight(),
                    footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
                    leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
                    contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

                // normalize content
                contentHeight -= $NAV_MENU.height() + footerHeight;

                $RIGHT_COL.css('min-height', contentHeight);
            };

            $rootScope.sideClick = function($event) {
                console.log("clicked " + $event.target);
                var $li = $($event.target).parent();
                console.log($li);
                if ($li.is('.active')) {
                    $li.removeClass('active active-sm');
                    $('ul:first', $li).slideUp(function() {
                        setContentHeight();
                    });
                } else {
                    // prevent closing menu if we are on child menu
                    if (!$li.parent().is('.child_menu')) {
                        $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                        $SIDEBAR_MENU.find('li ul').slideUp();
                    }

                    $li.addClass('active');

                    $('ul:first', $li).slideDown(function() {
                        setContentHeight();
                    });
                }

            }

            if(true) {
                return;
            }
            $rootScope.currenttime = new Date().getTime();
            $rootScope.metadata = appmetadata;
            $rootScope.apps = appmetadata.apps;
            var sidebarmetadata = [];
            var grpcound = 1;
            var lastgroup = null;
            $.each($rootScope.apps, function(i,val){
                if(i<3){
                    var appdef = {'isgroup' : false, 'appdef' : val };
                    sidebarmetadata.push(appdef);
                }else {
                    if(i % 4 == 0) {
                        var groupdef = {'isgroup' : true, 'groupid' : 'gp' + i, 'groupname' : 'Group ' + grpcound++, 'children' : []};
                        sidebarmetadata.push(groupdef);
                        lastgroup = groupdef;
                    }
                    var appdef = {'isgroup' : false, 'appdef' : val };
                    if(lastgroup) {
                        lastgroup.children.push(appdef);
                    }else {
                        //sidebarmetadata.push(appdef);
                    }
                }
            });
            $('#customstyle').html(appmetadata.customStyle);
            if(true) {
                return;
            }

            $http.jsonp(  "/metaData/index/test?callback=JSON_CALLBACK").
                success(function (data) {
                    $rootScope.metadata = data;
                    $rootScope.apps = data.apps;
                    console.log(data);
                    var sidebarmetadata = [];
                    var grpcound = 1;
                    var lastgroup = null;
                    $.each($rootScope.apps, function(i,val){
                       if(i<3){
                           var appdef = {'isgroup' : false, 'appdef' : val };
                           sidebarmetadata.push(appdef);
                       }else {
                           if(i % 4 == 0) {
                               var groupdef = {'isgroup' : true, 'groupid' : 'gp' + i, 'groupname' : 'Group ' + grpcound++, 'children' : []};
                               sidebarmetadata.push(groupdef);
                               lastgroup = groupdef;
                           }
                           var appdef = {'isgroup' : false, 'appdef' : val };
                           if(lastgroup) {
                               lastgroup.children.push(appdef);
                           }else {
                               //sidebarmetadata.push(appdef);
                           }
                       }
                    });
                    //$rootScope.sidebarmetadata = sidebarmetadata;
                    //console.log(JSON.stringify($rootScope.sidebarmetadata));
                    //var temp = '<li> <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> AK TESTING<span class="fa arrow"></span></a> <ul class="nav nav-second-level"> <li> <a href="flot.html">Flot Charts</a> </li> <li> <a href="morris.html">Morris.js Charts</a> </li> </ul> </li>';
                    //$("#side-menu").append(temp);
                   // $('.collapse').collapse();
                    $('#customstyle').html(data.customStyle);
                })
    }])
    .controller('WidgetCtrl', ['$scope', '$routeParams', '$compile', '$http', '$rootScope', '$sce', '$window', '$location',
        function ($scope, $routeParams, $compile, $http, $rootScope, $sce, $window, $location) {
            console.log($routeParams);
            $scope.init = function(appid, title, pageid) {
                $scope.appid = appid;
                $scope.title = title;



                try {
                    var appDefinition = appDef(appid);
                    $scope.appdef = appDefinition;
                    setTimeout(function () {
                        //$location.path("/home");
                        if(!pageid) {
                            pageid= appid;
                        }
                        var pageDefinition = pageDef(appDefinition, pageid);

                        $scope.appDisplayName = appDefinition.displayname;

                        var updatedprocessor = pageDefinition.pageprocessor.split("#appContent").join("#widget" + appid);
                        /*window.eval(pageDefinition.pageprocessor);*/
                        console.log(updatedprocessor);
                        window.eval(updatedprocessor);
                        var pageProcessorName = 'pageprocessor' + pageDefinition.pageid;
                        //Invoke the Pre processor
                        console.log("pageProcessorName : " + pageProcessorName);
                        if (window[pageProcessorName] != undefined) {
                            window[pageProcessorName](pageDefinition, $scope, $routeParams, $compile,
                                $http, $rootScope, $sce, $window, $location);
                            $scope.$apply();
                        }


                    },100);
                    //console.log(appDefinition);
                } catch (e) {
                    console.log(e);
                    $.unblockUI();
                    /*apprise("Unknown error occured while processing the request.!", {'verify': false, 'textYes': "Ok"}, function (r) {
                        //$rootScope.back();
                    });*/
                }

                /*var selectedApp = null;
                $.each(appmetadata.apps, function(i, val) {
                    console.log(val.name);
                    if(val.name == appid) {
                        selectedApp = val;
                    }
                });
                $scope.trustHtml = function(html) {
                    return $sce.trustAsHtml(html);
                }
                console.log("inside widget controller = " +  $scope.appid);
                if(selectedApp) {
                    $scope.datatemplate = selectedApp.pages[0].datatemplate;
                    if(!$scope.datatemplate) {
                        $scope.datatemplate = selectedApp.pages[0].pageTemplate;
                    }
                    var data = $compile('<p ng-bind-html="trustHtml(datatemplate);"> </p>')($scope);
                    $("#widget" + appid).append(data);

                }*/
            }
    }])
    .controller('DashboardCtrl', ['$scope', '$routeParams', '$compile', '$http', '$rootScope', '$sce', '$window', '$location',
        function ($scope, $routeParams, $compile, $http, $rootScope, $sce, $window, $location) {
            var dbid= $routeParams.dbid;
            var dboard = null;
            if(true) {
                console.log("dashboard");
                $http.get("/db.json").success(function (data) {
                    console.log(data);
                    $rootScope.dbdata = data;
                }).error(function (data) {
                    console.log("Error "  + data);
                });

                return;
            }
            $.each(dashboardMetadata.sidebar.navigation, function(i, val){
                console.log(val.type + " " + val.id);
                if(val.type == 'dashboard' && val.id == parseInt(dbid)) {
                    dboard = val;
                }
            });
            var db = "";
            if(dboard) {
                $.each(dboard.widgets, function(i, val){
                    db += '<div class="col-lg-' + val.span + '">' +
                        '<div ng-Controller="WidgetCtrl" ng-init="init(\'' + val.id + '\',\'' + val.title + '\');">' +
                        '<div class="panel chat-panel panel-default">' +
                        '<div class="panel-heading">' +
                        '<img ng-src="/metaData/appLogo/{{appdef.id}}" style="width:20px;"/>&nbsp;&nbsp; {{appDisplayName}}'+
                        '</div>' +
                        '<div class="panel-body" id="widget{{appdef.name}}" style="padding:0px;">' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                });


            }

            $scope.appidentifier = "Videos";

            $scope.apptitle="Videos";
            /*var db = '<div class="col-lg-4">' +
                    '<div ng-Controller="WidgetCtrl" ng-init="init(\'Videos\',\'Videos\')">' +
                    '<div class="panel chat-panel panel-default">' +
                    '<div class="panel-heading">' +
                    '<img ng-src="/metaData/appLogo/{{appdef.id}}" style="width:20px;"/>&nbsp;&nbsp; {{appDisplayName}}'+
                    '</div>' +
                    '<div class="panel-body" id="widget{{appdef.name}}" style="padding:0px;">' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            db = db + db.split("Videos").join("News");*/
            console.log(db);
            var data = $compile(db)($scope);
            $("#dashboardcontent").append(data);
            console.log("dashboard id : " + $routeParams.dbid);

    }])
    .controller('AppCtrl', ['$scope', '$routeParams', '$compile', '$http', '$rootScope', '$sce', '$window', '$location',
        function ($scope, $routeParams, $compile, $http, $rootScope, $sce, $window, $location) {
            //console.log("App control called");
            console.log($routeParams.appid + " : " + $routeParams.pageid);
            $rootScope.appid = $routeParams.appid;
            $rootScope.pageid = $routeParams.pageid;
            var pageDefinition = function (appDefinition, pageId) {
                var pageDef;
                var i;
                for (i = 0; i < appDefinition.pages.length; i++) {
                    var tempPage = appDefinition.pages[i]
                    if (tempPage.pageid == pageId) {
                        //alert (tempApp.name);
                        pageDef = tempPage;
                        break;
                    }
                }
                return pageDef;
            };
            $scope.init = function(app, appid1, pageid1) {
                console.log("Init called : " + appid1 + " : " + pageid1);
                console.log(app.title);
                if(app.appdef) {
                    setTimeout(function () {

                        console.log(app.appdef.name);
                        var pagedef = app.appdef.pages[0];
                        if(appid1 == app.appdef.name) {
                            console.log("FOUND : " + appid1);
                            pagedef = pageDefinition(app.appdef, pageid1);
                        }
                        var appid = app.appdef.name;
                        $("#widget" + appid).html("");
                        console.log("page Def : " + pagedef);
                        var updatedprocessor = pagedef.pageprocessor.split("#appContent").join("#widget" + appid);
                        window.eval(updatedprocessor);
                        var pageProcessorName = 'pageprocessor' + pagedef.pageid;

                        if (window[pageProcessorName] != undefined) {
                            window[pageProcessorName](pagedef, $scope, $routeParams, $compile,
                                $http, $rootScope, $sce, $window, $location);
                            $scope.$apply();
                        }
                    },100);
                }
            }
           /* var appid = $routeParams.appid;
            var pageid = $routeParams.pageid;
            console.log($routeParams);
            var appDefinition = appDef(appid);
            var apptitle = appDefinition.displayname;
            var db = '<div class="col-lg-10">' +
                '<div ng-Controller="WidgetCtrl" ng-init="init(\'' + appid + '\', \'' + apptitle + '\', \'' + pageid + '\');">' +
                '<div class="panel chat-panel panel-default">' +
                '<div class="panel-heading">' +
                '<img ng-src="/metaData/appLogo/{{appdef.id}}" style="width:20px;"/>&nbsp;&nbsp; {{appDisplayName}}'+
                '</div>' +
                '<div class="panel-body" id="widget{{appdef.name}}" style="padding:0px;">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
            var data = $compile(db)($scope);
            $("#dashboardcontent").append(data);*/

        }])
;
