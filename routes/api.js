/*
 * Serve JSON to our AngularJS client
 */
var fs = require('fs');
var extrequest = require('request');
var config = require('../config/config');

exports.name = function (req, res) {
  res.json({
    name: 'Bob'
  });
};

exports.metadata = function (req, res) {
  console.log("token inside metadata: " + req.user.token);
	var folder = "F:/AK/Projects/COMET/NodeProject/LGKPortal/";
	var name = req.params.name;
	var obj = JSON.parse(fs.readFileSync(folder + name + '.json', 'utf8'));
	res.json(obj);
};

exports.mykryptosapps = function (req, res) {
  console.log(config);
  var krptosurl = config.kryptosurl;
  console.log("url " +  krptosurl);
  var koptions = {
    url : krptosurl + "/api/mobapp/listMyApps?token=" + req.user.token,
  };
  extrequest.get(koptions, function(err, eresp, ebody) {
    console.log(ebody);

    res.json( JSON.parse( ebody));
  });
}

exports.kryptoslivedata = function (req, res) {
  var krptosurl = config.kryptosurl;
  var tenantid = req.params.tenantid;
  var koptions = {
    url : krptosurl + "/metagate/livedata/" +  tenantid ,
  };
  extrequest.get(koptions, function(err, eresp, ebody) {
    console.log(ebody);

    res.json( JSON.parse( ebody));
  });
}
