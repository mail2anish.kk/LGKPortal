
/*
 * GET home page.
 */

exports.index = function(req, res){
	console.log("Index called" + req.user.firstname);
  res.render('index', { title: 'Hey', message: 'Hello there!', username : req.user.firstname + " " + req.user.lastname });
};

exports.partials = function (req, res) {

  var name = req.params.name;
  console.log("Called partials " + name);
  res.render('partials/' + name);
};

exports.login = function(req, res){
	console.log("Login called");
  res.render('login');
};
