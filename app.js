
/**
 * Module dependencies
 */

var express = require('express'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  //errorHandler = require('error-handler'),
  morgan = require('morgan'),
  routes = require('./routes'),
  api = require('./routes/api'),
  http = require('http'),
  path = require('path'),
  passport = require('passport'),
  extrequest = require('request'),
  config = require("./config/config"),
  Strategy = require('passport-local').Strategy;

  // Configure the local strategy for use by Passport.
  //
  // The local strategy require a `verify` function which receives the credentials
  // (`username` and `password`) submitted by the user.  The function must verify
  // that the password is correct and then invoke `cb` with a user object, which
  // will be set at `req.user` in route handlers after authentication.
  passport.use(new Strategy(
    function(username, password, cb) {
      var kdata = {"username":username, "password":password };
      var koptions = {
				//url : "https://kryptos.kryptosmobile.com/api/authenticate",
        url : config.kryptosurl + "/api/authenticate",
				body : JSON.stringify(kdata)
			}
      extrequest.post(koptions, function(err, eresp, ebody) {
        if (!err && eresp.statusCode == 200) {
          var respbody = JSON.parse(ebody);
          console.log(respbody.firstname);
          return cb(null,respbody);
        }else {
          return cb(null,false);
        }

			});
      // if(username == password) {
      //   var user = {'username' : username, 'password' : password, 'id' : 10001};
      //   return cb(null,user);
      // }else {
      //   return cb(null,false);
      // }
      // db.users.findByUsername(username, function(err, user) {
      //   if (err) { return cb(err); }
      //   if (!user) { return cb(null, false); }
      //   if (user.password != password) { return cb(null, false); }
      //   return cb(null, user);
      // });
  }));

  // Configure Passport authenticated session persistence.
  //
  // In order to restore authentication state across HTTP requests, Passport needs
  // to serialize users into and deserialize users out of the session.  The
  // typical implementation of this is as simple as supplying the user ID when
  // serializing, and querying the user record by ID from the database when
  // deserializing.
  passport.serializeUser(function(user, cb) {
    cb(null, user.token);
  });

  passport.deserializeUser(function(id, cb) {
    var koptions = {
      url : config.kryptosurl + "/api/validatetoken?token=" + id,
    }
    extrequest.get(koptions, function(err, eresp, ebody) {

      console.log(ebody);
      if (!err && eresp.statusCode == 200) {
        var respbody = JSON.parse(ebody);
        return cb(null,respbody);
      }else {
        return cb(null,false);
      }
    });
    // console.log("deserialize called.." + id);
    // var user = {'username' : 'test', 'password' : 'test', 'id' : 10001};
    // cb(null, user);
    // db.users.findById(id, function (err, user) {
    //   if (err) { return cb(err); }
    //   cb(null, user);
    // });
  });
var app = module.exports = express();


/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 3010);
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
app.use(morgan('dev'));
app.use(bodyParser());
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false, cookie: { maxAge: 3600000 } }));

// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());

var env = process.env.NODE_ENV || 'development';

// development only
if (env === 'development') {
  //app.use(express.errorHandler());
}

// production only
if (env === 'production') {
  // TODO
}


/**
 * Routes
 */

// serve index and view partials
app.get('/', require('connect-ensure-login').ensureLoggedIn(), routes.index);
app.get('/partials/:name', require('connect-ensure-login').ensureLoggedIn(), routes.partials);
app.get('/login', routes.login);

app.post('/login',
  passport.authenticate('local', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
  });

app.get('/logout',
  function(req, res){
    req.logout();
    res.redirect('/');
});

// JSON API
app.get('/api/name', require('connect-ensure-login').ensureLoggedIn(), api.name);
app.get('/api/metadata/:name', require('connect-ensure-login').ensureLoggedIn(), api.metadata);
app.get('/api/kryptos/listMyApps', require('connect-ensure-login').ensureLoggedIn(), api.mykryptosapps);
app.get('/api/kryptos/livedata/:tenantid', require('connect-ensure-login').ensureLoggedIn(), api.kryptoslivedata);

// redirect all others to the index (HTML5 history)
app.get('*', routes.index);


/**
 * Start Server
 */

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
